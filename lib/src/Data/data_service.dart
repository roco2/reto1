import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:reto1/src/Data/url.dart';

class DataService {
  static Future<List<dynamic>> getUserList() async {
    try {
      final response = await http.get('${Url.BASE_RETO1}/photos');
      if (response.statusCode == 200) {
        return json.decode(response.body);
      } else {
        return null;
      }
    } catch (ex) {
      return null;
    }
  }
}
