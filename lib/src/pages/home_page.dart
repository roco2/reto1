import 'package:flutter/material.dart';
import 'package:reto1/src/Data/data_service.dart';


class HomePage extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title:Text('Reto Uno'),
        centerTitle: true,
        ),
        body: FutureBuilder(
          future: DataService.getUserList(),
          builder: (context, snapshot){
            if (snapshot.connectionState == ConnectionState.done) {
              final dta = snapshot.data;
              return ListView.separated(
                separatorBuilder: (context, index){
                    return Divider(height: 1,color: Colors.grey);
                },
                itemBuilder: (context, index){
                    return ListTile(
                      title: Text(
                        dta[index]['title'],
                      ),
                      subtitle: Image.network(
                        dta[index]['thumbnailUrl'],
                      ),
                    );
                },
                itemCount: dta.length,
              );
            }
            return Center(child: CircularProgressIndicator(),);
          },
        ),
    );
  }
}